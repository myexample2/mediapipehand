package com.example.mediapipeex.permission;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.mediapipeex.MainActivity;

public class PermissionActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    // 권한 승인 코드
    private int RESULT_PERMISSIONS = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        // 안드로이드 6.0 이상 버전에서는 CAMERA 권한 허가를 요청한다.
        requestPermissionCamera();
    }

    /**
     * 카메라 권한 요청
     *
     * @return 획득 값
     */
    public void requestPermissionCamera() {
        int sdkVersion = Build.VERSION.SDK_INT;
        Log.d(TAG, "sdkVersion : " + sdkVersion);
        if (sdkVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(PermissionActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        RESULT_PERMISSIONS);

            } else {
                startMainActivity();
            }
        } else {  // version 6 이하일때
            startMainActivity();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult Act");

        if (RESULT_PERMISSIONS == requestCode) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startMainActivity();
                // 권한 허가시
            } else {
                Toast.makeText(getApplicationContext(), "[설정] > [권한] 에서 권한을 허용해야 사용할 수 있습니다.", Toast.LENGTH_SHORT).show();
                // 권한 거부시
            }
            return;
        }
    }

    /**
     * Start login activity.
     */
    public void startMainActivity() {
        Intent intent = new Intent(PermissionActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
